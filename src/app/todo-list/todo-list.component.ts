import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { State } from "src/store/reducers";
import { DisplayRandomName, FetchData } from "src/store/actions";
import { Observable } from "rxjs";

@Component({
  selector: "app-todo-list",
  templateUrl: "./todo-list.component.html",
  styleUrls: ["./todo-list.component.scss"]
})
export class TodoListComponent implements OnInit {
  randomName$: Observable<string>;
  data$: Observable<object>;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.randomName$ = this.store.select<string>(state => state.todos.name);
    this.data$ = this.store.select<object>(state => state.todos.data);
  }

  onSyncAction() {
    this.store.dispatch(new DisplayRandomName());
  }

  onAsyncAction() {
    this.store.dispatch(new FetchData());
  }
}
